const axios = require('axios');

const gUtil = require('./google_util');

const responses = [];
const ctx = '';

const result = function () {
  return new Promise((resolve, reject) => {
    let data = '';
    let config = {
      method: 'get',
      url: `https://api.bitbucket.org/2.0/repositories/oneoriginus/sia-backend-core/pullrequests/activity?&pagelen=50&ctx=${ctx}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Basic U2thbmRhX3NocmVlOnNrYW5kYTIw',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        responses = responses.concat(response.data.values);
        ctx = response.data.next.match(/ctx=(.*)/)[0].replace('ctx=', '');
        resolve();
      })
      .catch(function (error) {
        console.log(error);
        reject();
      });
  });
};

async function getData() {
  try {
    let merged = {};
    let idTracker = {};
    let approved = {};
    let comments = {};

    for (let i = 0; i <= 7; i++) {
      await result();
      i++;
    }
    let prevDate = new Date().getTime() - 604800000;

    responses = responses.filter((e) => {
      for (let k in e) {
        if (
          e[k].date &&
          e[k].date > new Date(prevDate).toISOString().slice(0, 10) &&
          e[k].date.slice(0, 10) < new Date().toISOString().slice(0, 10)
        )
          return e;
      }
    });

    for (let i in responses) {
      if (responses[i].update && responses[i].update.state === 'MERGED') {
        if (merged[responses[i].update.author.display_name]) {
          merged[responses[i].update.author.display_name] = merged[responses[i].update.author.display_name] + 1;
        } else {
          merged[responses[i].update.author.display_name] = 1;
        }

        idTracker[responses[i].pull_request.id] = responses[i].update.date;
      }
    }
    for (let i in responses) {
      if (responses[i].approval && responses[i].approval.date < idTracker[responses[i].pull_request.id]) {
        if (approved[responses[i].approval.user.display_name]) {
          approved[responses[i].approval.user.display_name] = approved[responses[i].approval.user.display_name] + 1;
        } else {
          approved[responses[i].approval.user.display_name] = 1;
        }
      }
      if (responses[i].comment) {
        if (comments[responses[i].comment.user.display_name]) {
          comments[responses[i].comment.user.display_name] = comments[responses[i].comment.user.display_name] + 1;
        } else {
          comments[responses[i].comment.user.display_name] = 1;
        }
      }
    }
    let finalJson = {
      Date: new Date(prevDate).toISOString().slice(0, 10) + ' to ' + new Date().toISOString().slice(0, 10),
      approved: Object.entries(approved).join(';').replace(/,/g, ' - '),
      merged: Object.entries(merged).join(';').replace(/,/g, ' - '),
      comments: Object.entries(comments).join(';').replace(/,/g, ' - '),
    };
    await gUtil.appendToSheets(finalJson, () => console.log('Sheets updated'));
  } catch (error) {
    console.log(error);
  }
}

setInterval(getData, 604800);
