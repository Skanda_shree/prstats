const googleService = require("./google");
let creds = require("./creds.json");

module.exports = {
  async appendToSheets(json, cb) {
    try {
      let spreadsheetId = "1Zx8kCU5hlek6hfM7z7vf2zrbzpIJ23lIiceYkzUpTLw",
        sheetName = "Sheet26";
      let data = {};
      var doc = await googleService.getDocument(spreadsheetId);

      await googleService.loadCredentials(doc, creds);
      await googleService.getInfo(doc);
      data.worksheets = doc.sheetsByIndex;

      if (data && data.worksheets && data.worksheets.length > 0) {
        let sheet = data.worksheets.filter(e => e.title === sheetName);

        if (sheet.length > 0) {
          sheet = sheet[0];

          await sheet.addRow(json);
        }
      }
      cb();
    } catch (err) {
      console.log("append to sheets", err);
    }
  }
};
