const GoogleSpreadsheet = require("google-spreadsheet");

const getDocument = async function(spreadsheetId) {
  return new GoogleSpreadsheet.GoogleSpreadsheet(spreadsheetId);
};

const loadCredentials = function(document, creds) {
  return new Promise((resolve, reject) => {
    document.useServiceAccountAuth(creds, err => {
      if (err) return reject(err);
    });
    resolve();
  });
};

const getInfo = async function(document) {
  try {
    await document.loadInfo();
  } catch (err) {
    console.log(err);
  }
};

const getRows = async function(sheet) {
  try {
    return await sheet.getRows({});
  } catch (err) {
    console.log(err);
  }
};

module.exports.getDocument = getDocument;
module.exports.loadCredentials = loadCredentials;
module.exports.getInfo = getInfo;
module.exports.getRows = getRows;
